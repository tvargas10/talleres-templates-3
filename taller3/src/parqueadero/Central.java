package parqueadero;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import Estructuras.Queue;
import Estructuras.Stack;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Queue colaEspera;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Stack pila1;
	private Stack pila2;
	private Stack pila3;
	private Stack pila4;
	private Stack pila5;
	private Stack pila6;
	private Stack pila7;
	private Stack pila8;
    
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
   private Stack pilaTemp;
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
        colaEspera = new Queue();
        pila1 = new Stack();
        pila2 = new Stack();
        pila3 = new Stack();
        pila4 = new Stack();
        pila5 = new Stack();
        pila6 = new Stack();
        pila7 = new Stack();
        pila8 = new Stack();
        pilaTemp = new Stack();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro nuevo = new Carro(pColor, pMatricula, pNombreConductor);
    	colaEspera.enqueue(nuevo);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
      Carro a = (Carro) colaEspera.first();
      String matricula = a.darMatricula();
      colaEspera.dequeue();
      String ubic = parquearCarro(a);
      String resp = "vehiculo con matr�cula " + matricula + " ubicado en " + ubic;
      return resp;
      
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	try
    	{
    		Carro a = sacarCarro(matricula);
    		return cobrarTarifa(a);
    	}
    	catch (Exception e)
    	{
    		e.getMessage();
    		return 0.0;
    	}
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	String pila = "";
        for (int i = 0; i <= 8; i++)
        {
      	  if(i == 1 && pila1.size() < 4)
      	  {
      		  pila1.push(aParquear);
      		  pila.equals(pila1);
      	  }
      	  else if(i == 2 && pila2.size() < 4)
      	  {
      		  pila2.push(aParquear);
      		  pila.equals(pila2);
      	  }
      	  else if(i == 3 && pila3.size() < 4)
      	  {
      		  pila3.push(aParquear);
      		  pila.equals(pila3);
      	  }
      	  else if(i == 4 && pila4.size() < 4)
      	  {
      		  pila4.push(aParquear);
      		  pila.equals(pila4);
      	  }
      	  else if(i == 5 && pila5.size() < 4)
      	  {
      		  pila5.push(aParquear);
      		pila.equals(pila5);
      	  }
      	  else if(i == 6 && pila6.size() < 4)
      	  {
      		  pila6.push(aParquear);
      		  pila.equals(pila6);
      	  }
      	  else if(i == 7 && pila7.size() < 4)
      	  {
      		  pila7.push(aParquear);
      		  pila.equals(pila7);
      	  }
      	  else if(i == 8 && pila8.size() < 4)
      	  {
      		  pila.equals(pila8);
      		  pila8.push(aParquear);
      	  }
      	  else
      	  {
      		  Exception e = new Exception();
      		  throw e;
      	  }
        }
        return pila;
      }     	    
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	Stack pila = null;
    	int i = 0;
    	if (i == 1) 
    	{
    		pila.equals(pila1);
    	}
    	if (i == 2) 
    	{
    		pila.equals(pila2);
    	}
    	if (i == 3) 
    	{
    		pila.equals(pila3);
    	}
    	if (i == 4) 
    	{
    		pila.equals(pila4);
    	}
    	if (i == 5) 
    	{
    		pila.equals(pila5);
    	}
    	if (i == 6) 
    	{
    		pila.equals(pila6);
    	}
    	if (i == 7) 
    	{
    		pila.equals(pila7);
    	}
    	if (i == 8) 
    	{
    		pila.equals(pila8);
    	}
    
    	for (i = 1; i <= 8; i++)
    	{
    		while (!pila.isEmpty())
    		{
    			Carro a = (Carro) pila.Top();
    			if (a.darMatricula().equals(matricula))
    			{
    				Carro sacar = (Carro) pila.pop();
    				while(!pila.isEmpty())
    				{
    					pila.push(pilaTemp.pop());
    				}
    				return sacar;
    			}
    			else
    			{
    				pilaTemp.push(pila.pop());
    			}
    		}
    		while(!pilaTemp.isEmpty())
    		{
    			pila.push(pilaTemp.pop());
    		}
    	}
    	return null;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro a = (Carro) pila1.Top();
    	while(a.darMatricula() != matricula && pila1.iterator().hasNext()==true)
    	{
    		pila1.pop();
    	}
    	return a;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro a = (Carro) pila2.Top();
    	while(a.darMatricula() != matricula && pila2.iterator().hasNext()==true)
    	{
    		pila2.pop();
    	}
    	return a;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro a = (Carro) pila3.Top();
    	while(a.darMatricula() != matricula && pila3.iterator().hasNext()==true)
    	{
    		pila3.pop();
    	}
    	return a;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	Carro a = (Carro) pila4.Top();
    	while(a.darMatricula() != matricula && pila4.iterator().hasNext()==true)
    	{
    		pila4.pop();
    	}
    	return a;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro a = (Carro) pila5.Top();
    	while(a.darMatricula() != matricula && pila5.iterator().hasNext()==true)
    	{
    		pila5.pop();
    	}
    	return a;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	Carro a = (Carro) pila6.Top();
    	while(a.darMatricula() != matricula && pila6.iterator().hasNext()==true)
    	{
    		pila6.pop();
    	}
    	return a;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	Carro a = (Carro) pila7.Top();
    	while(a.darMatricula() != matricula && pila7.iterator().hasNext()==true)
    	{
    		pila7.pop();
    	}
    	return a;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	Carro a = (Carro) pila8.Top();
    	while(a.darMatricula() != matricula && pila8.iterator().hasNext()==true)
    	{
    		pila8.pop();
    	}
    	return a;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	long tiempo = car.darLlegada();
    	double min = tiempo/60000;
    	double dinero = min*25;
    	return dinero;
    }
    
    /**
     * Retorna el tama�o de las pilas del parqueadero
     * @return N
     */
    public int darTama�o(int i)
    {
    	Stack pila = null;
    	
    	if (i == 1) 
    	{
    		pila.equals(pila1);
    	}
    	else if (i == 2) 
    	{
    		pila.equals(pila2);
    	}
    	else if (i == 3) 
    	{
    		pila.equals(pila3);
    	}
    	else if (i == 4) 
    	{
    		pila.equals(pila4);
    	}
    	else if (i == 5) 
    	{
    		pila.equals(pila5);
    	}
    	else if (i == 6) 
    	{
    		pila.equals(pila6);
    	}
    	else if (i == 7) 
    	{
    		pila.equals(pila7);
    	}
    	else if (i == 8) 
    	{
    		pila.equals(pila8);
    	}
    	return pila.size();
    }
    
    /**
     * Retorna el tama�o de la cola de carros en espera
     * @return i
     */
    public int tamTemp()
    {
    	int i = colaEspera.size();
    	return i;
    }
}
