package Estructuras;

public class Queue <Item>
{
	private Node first;	//Link al �ltimo nodo a�adido
	private Node last;	//Link al nodo a�adido m�s reciente
	private int N;		//N�mero de elementos en el queque
	
	private class Node
	{
		// Clase para definir el nodo
		Item item;
		Node next;
	}
	
	public boolean isEmpty() { return first == null; }	//O: N == 0.
	public int size() { return N; }
	
	public void enqueue(Item item)
	{	//A�adir elemento al final de la lista
		Node oldLast = last;
		last = new Node();
		last.item = item;
		last.next = null;
		if (isEmpty()) first = last;
		else		oldLast.next = last;
		N++;
	}
	
	public Item dequeue()
	{	//Remueve el elemento al inicio de la lista
		Item item = first.item;
		first = first.next;
		if (isEmpty()) last = null;
		N--;
		return item;
	}
	
	public Item first()
	{
		return (Item) first;
	}
}

// Tomado de Algorithms 4th Ed - Robert Sedgewick, Kevin Wayne
