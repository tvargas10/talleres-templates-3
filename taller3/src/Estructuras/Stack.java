package Estructuras;

import java.util.Iterator;

public class Stack <Item> implements Iterable<Item>
{
	/**
	 * �ltimo nodo a�adido
	 */
	private Node first;	
	
	/**
	 * N�mero de elementos en la cola
	 */
	private int N;		
	
	/**
	 * Clase que define el Nodo
	 */
	private class Node
	{
		// Clase para definir el nodo
		Item item;
		Node next;
	}
	
	/**
	 * Indica si la cola esta vac�a o no
	 * @return true si first == null, false de lo contrario
	 */
	public boolean isEmpty() { return first == null; }	//O: N == 0.
	
	/**
	 * retorna el tama�o de la cola
	 * @return N: n�mero de elementos en la cola
	 */
	public int size() { return N; }
	
	public void push(Item item)
	{	//A�adir elemento al inicio de la lista
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
		N++;
	}
	
	public Item pop()
	{	//Remueve el elemento al inicio de la lista
		Item item = first.item;
		first = first.next;
		N--;
		return item;
	}
	
	public Item Top()
	{
		return (Item) first;
	}
	
	public Iterator<Item> iterator()
	{
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item>
	{
		private Node current = first;
		
		public boolean hasNext() {return current != null;}
		
		public void remove() {}
		
		public Item next()
		{
			Item item = current.item;
			current = current.next;
			return item;
		}
		
	}
}

//Tomado de Algorithms 4th Ed - Robert Sedgewick, Kevin Wayne