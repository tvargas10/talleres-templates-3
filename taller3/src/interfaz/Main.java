package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
		System.out.println("* * * * * * * * * * * * * * * * ");
		System.out.println("<< Registrar Cliente >>\n");
		String mensaje =
				"Escriba el color del carro\n";

		System.out.print(mensaje);
		String pColor = br.readLine();
		
		String mensaje2 = "Ingrese la matricula del vehiculo";
		System.out.print(mensaje2);
		String pMatricula = br.readLine();
		
		String mensaje3 = "Ingrese el nombre del conductor";
		System.out.print(mensaje3);
		String pNombreConductor = br.readLine();
		
		central.registrarCliente(pColor, pMatricula, pNombreConductor);
		System.out.println("Se registro el vehiculo de color " + pColor + " con matricula" + pMatricula + " propiedad de " + pNombreConductor + " .");
		
  }
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  String a = central.parquearCarroEnCola();
	  System.out.print("Se parqueo el " + a);
	  
  }
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("Ingrese la matr�cula del vehiculo que va a salir");
	  String matricula = br.readLine();
	  double x = central.atenderClienteSale(matricula);
	  System.out.println("Salio el carro de matricula " + matricula + "y se le cobro: " + x);
  }
  /**
   * M�todo que permite visualizar graficamente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  int N = 0;
	  String msg0 = "";
	  String msg1 = "*";
	  String msg2 = "**";
	  String msg3 = "***";
	  String msg4 = "****";
	  
	  for(int i = 1;i<=8;i++)
	  {
		  N = central.darTama�o(i);
		  System.out.println("Pila" + i);
		  if(N == 0)
		  {
			  System.out.println(msg0);
		  }
		  if(N == 1)
		  {
			  System.out.println(msg1);
		  }
		  else if(N == 2)
		  {
			  System.out.println(msg2);
		  }
		  else if(N == 3)
		  {
			  System.out.println(msg3);
		  }
		  else if(N == 4)
		  {
			  System.out.println(msg4);
		  }
	  }
	  
  }
  
  /**
   * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("<< Consultar estado de cola >>\n");
	  int i = central.tamTemp();
	  System.out.println("Hay " + i + " carros en total esperando a ser parqueados");
  }
  
  /**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
