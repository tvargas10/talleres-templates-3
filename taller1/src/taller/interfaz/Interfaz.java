package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Número de filas
	 */
	private int x;
	
	/**
	 * Número de columnas
	 */
	private int y;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
       //TODO
		imprimirMenu();
		int mod = sc.nextInt();
		if (mod == 1)
		{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("Juego entre amigos");
		System.out.println("Ingrese la cantidad de filas con las que desea jugar");
		x=Integer.parseInt(sc.next());
		System.out.println("Ingrese la cantidad de columnas con las que desea jugar");
		y=Integer.parseInt(sc.next());
		
		if (x<4 || y<4)
		{
			System.out.println("El número de filas y columnas no puede ser menor a cuatro");
			empezarJuego();
		}
		else
		{
			System.out.println("Nombre del jugador 1");
			String nom1 = sc.next();
			System.out.println("Simbolo");
			String sim1 = sc.next();
			System.out.println("Nombre del jugador 2");
			String nom2 = sc.next();
			System.out.println("Simbolo");
			String sim2 = sc.next();
			ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
			jugadores.add(new Jugador(nom1, sim1));
			jugadores.add(new Jugador(nom2, sim2));
			juego = new LineaCuatro(jugadores, x, y);
			String[][] tablero = juego.darTablero();
			for (int i = 0; i < tablero.length; i++)
			{
				String linea = "";
				for (int j = 0; j < tablero[0].length;j++)
				{
					linea = linea + tablero[i][j] + "";
				}
				System.out.println(linea);
			}
		}
		juego();
		}
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		imprimirMenu();
		int mod = sc.nextInt();
		if (mod == 2)
		{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("Juego entre amigos");
		System.out.println("Ingrese la cantidad de filas con las que desea jugar");
		x=Integer.parseInt(sc.next());
		System.out.println("Ingrese la cantidad de columnas con las que desea jugar");
		y=Integer.parseInt(sc.next());
		
		if (x<4 || y<4)
		{
			System.out.println("El número de filas y columnas no puede ser menor a cuatro");
			empezarJuego();
		}
		else
		{
			System.out.println("Nombre del jugador 1");
			String nom1 = sc.next();
			System.out.println("Simbolo");
			String sim1 = sc.next();
			System.out.println("Nombre de la maquina");
			String nom2 = sc.next();
			System.out.println("Simbolo");
			String sim2 = sc.next();
			ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
			jugadores.add(new Jugador(nom1, sim1));
			jugadores.add(new Jugador(nom2, sim2));
			juego = new LineaCuatro(jugadores, x, y);
			String[][] tablero = juego.darTablero();
			for (int i = 0; i < tablero.length; i++)
			{
				String linea = "";
				for (int j = 0; j < tablero[0].length;j++)
				{
					linea = linea + tablero[i][j] + "";
				}
				System.out.println(linea);
			}
		}
		juegoMaquina();
		}
		
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
		System.out.println("Seleccione la columna a jugar");
		int c = sc.nextInt();
		System.out.println("Seleccione la fila a jugar");
		int f = sc.nextInt();
		juego.registrarJugada(f, c);
		if (juego.registrarJugada(f, c) == true)
		{
		juego.registrarJugadaAleatoria();
		}
		else
		{
			System.out.println("Jugada Invalida");
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		System.out.println("¿Cuantas columnas desea en su tablero?");
		int col = sc.nextInt();
		System.out.println("¿Cuantas filas desea en su tablero?");
		int fil = sc.nextInt();
		String[][] tablero = juego.darTablero();
		for (int i = 0; i < tablero.length; i++)
		{
			String linea = "";
			for (int j = 0; j < tablero[0].length;j++)
			{
				linea = linea + tablero[i][j] + "";
			}
			System.out.println(linea);
		}
	}
}
