package mundo;

import java.util.Date;

public class Cliente extends Trabajo 
{
	public enum Miembresitos
	{
		REGIONAL,
		NACIONAL,
		INTERNACIONAL, 
		OTROS
	}
	
	protected Miembresitos miembros;

	public Cliente(Date pFecha, String pLugar, tipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal, Miembresitos pMiembro) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		miembros = pMiembro;
	}
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con FAMILIA: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}
}
