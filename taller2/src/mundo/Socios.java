package mundo;

import java.util.Date;

public class Socios extends Trabajo
{
	public enum Miembro
	{
		COMPAÑIA,
		COMPAÑIA_EXTERNA_NACIONAL,
		COMPAÑIA_INTERNACIONAL, 
		OTROS
	}
	
	protected Miembro miembros;

	public Socios(Date pFecha, String pLugar, tipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal, Miembro pMiembro) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		miembros = pMiembro;
	}
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con FAMILIA: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}
}
