package mundo;

import java.util.Date;

public class Trabajo extends Evento 
{
	public enum tipoEventoTrabajo
	{
		JUNTAS,
		ALMUERZO,
		CENA,
		COCTEL,
		PRESENTACION,
	}
	
	protected tipoEventoTrabajo tipo;
	
	public Trabajo(Date pFecha, String pLugar, tipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo = pTipo;
	}
}



